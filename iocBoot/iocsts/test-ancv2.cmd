#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## test env
epicsEnvSet ("DEVIP", "172.17.10.62")
epicsEnvSet ("DEVPORT", "2101")
epicsEnvSet ("IOCBL", "PINK")
epicsEnvSet ("IOCDEV", "ANC01")

## macros
epicsEnvSet ("PORT", "ANCP")
epicsEnvSet ("PORT2", "ANCP2")

drvAsynIPPortConfigure("$(PORT)","$(DEVIP):$(DEVPORT)",0,0,0)

## anc350AsynMotorCreate( char *port, int addr, int card, int nAxes )
anc350AsynMotorCreate("$(PORT)", "0", "0", "1")

## drvAsynMotorConfigure(const char *portName, const char *driverName, int card, int num_axes)
#drvAsynMotorConfigure("$(PORT2)", "anc350AsynMotor", "0", "1")

## Load record instances
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=$(IOCBL):$(IOCDEV):,R=asyn,PORT=$(PORT),ADDR=0,OMAX=256,IMAX=256")
dbLoadRecords("$(ANC350)/db/ancController.template","P=$(IOCBL):$(IOCDEV),PORT=$(PORT)")
#ACT0
dbLoadRecords("$(ANC350)/db/ancStepModule.template","P=$(IOCBL):$(IOCDEV),PORT=$(PORT),ADDR=0")
dbLoadRecords("${TOP}/iocBoot/${IOC}/ancsup.db","BL=$(IOCBL),DEV=$(IOCDEV),CH=ACT0")
dbLoadRecords("${TOP}/iocBoot/${IOC}/ancchannelsup.db","P=$(IOCBL):$(IOCDEV),PORT=$(PORT),ADDR=0")

cd "${TOP}/iocBoot/${IOC}"

## AUTOSAVE
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

## AUTOSAVE
create_monitor_set("auto_settings.req", 30, "BL=$(IOCBL),DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
